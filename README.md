# Git Repository Monitor
This package provides a simple script-based Git repository monitoring solution. It allows you to monitor multiple Git repositories for changes in the upstream and HEAD commits.

## Installation:
1. Clone this repository:
   ```sh
   git clone <repository-url>
   ```

2. Navigate to the repository directory:
   ```sh
   cd git-repository-monitor
   ```

3. Run the installation script as root:
   ```sh
   sudo ./install.sh
   ```
   This will install the necessary scripts, set up the service, and create the repository file.

4. Start monitoring your Git repositories!

## Usage:
### Adding a Repository:
```sh
   git-monitor-client --add /path/to/repo
```

### Removing a Repository:
To remove a monitored repository, use the `git-monitor-client` command with the `remove` option followed by the repository path:
```sh
git-monitor-client --remove /path/to/repo
```

### Checking Server Status:
To check the status of the server, use the `git-monitor-client` command with the `status` option:
```sh
git-monitor-client --status
```

## Uninstallation:
To uninstall the package and remove all associated files, run the uninstallation script as root:
```sh
sudo ./uninstall.sh
```

This will stop the service, remove the symlinks, and optionally remove the repository file (prompted during uninstallation).

## Files:
- server.py: The server script that monitors the repositories.
- client.py: The client script used to add or remove repositories and check server status.
- install.sh: The installation script that sets up the environment and creates necessary symlinks.
- uninstall.sh: The uninstallation script that removes the package and cleans up the environment.
- README.md: This readme file.

## Copyright
AceldamA (C) 2023

## License:
This project is licensed under the MIT License.
