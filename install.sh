#!/bin/bash

# Check if running as root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root."
   exit 1
fi

# Set the directory where the scripts are located
SCRIPT_DIR="/opt/git-monitor"

# Set the service name
SERVICE_NAME="git-monitor"

# Set the path to the service file
SERVICE_FILE="/etc/systemd/system/$SERVICE_NAME.service"

# Set the path to the repository file
REPO_FILE="/etc/git-monitor/repos.txt"

# Check if Python is installed
if ! command -v python &> /dev/null; then
    echo "Python is not installed. Installing..."
    apt update
    apt install -y python
fi

# Check if Git is installed
if ! command -v git &> /dev/null; then
    echo "Git is not installed. Installing..."
    apt update
    apt install -y git
fi

# Create the script directory
mkdir -p $SCRIPT_DIR

# Copy the server and client scripts to the script directory
cp server.py $SCRIPT_DIR
cp client.py $SCRIPT_DIR

# Create necessary symlinks
ln -sfn $SCRIPT_DIR/server.py /usr/local/bin/git-monitor-server
ln -sfn $SCRIPT_DIR/client.py /usr/local/bin/git-monitor-client

# Create the repository file
touch $REPO_FILE

# Create the service file
echo "[Unit]
Description=Git Repository Monitor
After=network.target

[Service]
ExecStart=/usr/bin/python $SCRIPT_DIR/server.py -f $REPO_FILE
WorkingDirectory=$SCRIPT_DIR
Restart=always

[Install]
WantedBy=multi-user.target" > $SERVICE_FILE

# Reload systemd to ensure it detects the new service
systemctl daemon-reload

# Add permissions to run git-monitor-client without sudo
chmod +x $SCRIPT_DIR/client.py
chown root:$USER $SCRIPT_DIR/client.py
chmod u+s $SCRIPT_DIR/client.py

# Enable and start the service
systemctl enable $SERVICE_NAME
systemctl start $SERVICE_NAME

echo "Installation completed."
