import socket
import argparse

def send_command(command):
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        client_socket.connect(('localhost', 5000))
        client_socket.send(command.encode())
        client_socket.close()
    except ConnectionRefusedError:
        print("Server is not running.")

def check_server_status():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = client_socket.connect_ex(('localhost', 5000))
    if result == 0:
        print("Server is running.")
    else:
        print("Server is not running.")

# Usage: python client.py [-a <repo_path> | -r <repo_path> | -s]
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Git Repository Manager')
    parser.add_argument('-a', '--add', metavar='repo_path', type=str, help='Add repository to monitor')
    parser.add_argument('-r', '--remove', metavar='repo_path', type=str, help='Remove repository from monitoring')
    parser.add_argument('-s', '--status', action='store_true', help='Check server status')
    args = parser.parse_args()

    if args.status:
        check_server_status()
    elif args.add:
        send_command(f"+ {args.add}")
        print(f"Repo: {args.add} sent to the running instance.")
    elif args.remove:
        send_command(f"- {args.remove}")
        print(f"Repo: {args.remove} removal sent to the running instance.")
    else:
        parser.print_help()
