import os
import subprocess
import threading
import time
import argparse

def get_git_output(repo_path, args):
    os.chdir(repo_path)
    result = subprocess.run(['git'] + args, capture_output=True, text=True)
    if result.returncode == 0:
        return result.stdout.strip()
    else:
        print(f"Git command failed: {result.stderr}")
        return None

def compare_upstream_and_head(repo_path):
    upstream_hash = get_git_output(repo_path, ['rev-parse', '@{upstream}'])
    head_hash = get_git_output(repo_path, ['rev-parse', 'HEAD'])

    if upstream_hash and head_hash:
        if upstream_hash == head_hash:
            print(f"Repo: {repo_path} - Upstream and HEAD are the same.")
        else:
            print(f"Repo: {repo_path} - Upstream and HEAD are different.")
            print(f"Upstream commit: {upstream_hash}")
            print(f"HEAD commit: {head_hash}")

def check_repos(repos, port):
    while True:
        for repo_path in repos:
            compare_upstream_and_head(repo_path)
        print("--- Repos checked ---")
        time.sleep(10)

def add_repo(repo_path, repos_file, port):
    with open(repos_file, 'a') as f:
        f.write(f"{repo_path}:{port}\n")
    print(f"Repo: {repo_path} added.")

def remove_repo(repo_path, repos_file):
    with open(repos_file, 'r') as f:
        lines = f.readlines()
    with open(repos_file, 'w') as f:
        for line in lines:
            if not line.startswith(repo_path + ":"):
                f.write(line)
    print(f"Repo: {repo_path} removed.")

# Usage: python server.py -f <repos_file> -p <port>
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Git Repository Checker')
    parser.add_argument('-f', '--file', metavar='repos_file', type=str, help='File to store monitored repositories')
    parser.add_argument('-p', '--port', metavar='port', type=int, default=5000, help='Port number for IPC')
    args = parser.parse_args()

    repos = []
    threads = []

    if not args.file:
        print("Please provide the file to store the monitored repositories.")
    else:
        repos_file = args.file

        if os.path.isfile(repos_file):
            with open(repos_file, 'r') as f:
                for line in f:
                    line_parts = line.strip().split(":")
                    if len(line_parts) == 2:
                        repo_path, port = line_parts
                        if not os.path.isdir(repo_path):
                            print(f"Repo: {repo_path} does not exist. Skipping...")
                            continue
                        t = threading.Thread(target=check_repos, args=([repo_path], int(port)))
                        t.start()
                        threads.append(t)

        while True:
            command = input("Enter '+ <repo_path>' to add a repository or '- <repo_path>' to remove a repository: ")
            command_parts = command.split()
            if len(command_parts) != 2 or command_parts[0] not in ['+', '-']:
                print("Invalid command.")
                continue
            if command_parts[0] == '+':
                add_repo(command_parts[1], repos_file, args.port)
                t = threading.Thread(target=check_repos, args=([command_parts[1]], args.port))
                t.start()
                threads.append(t)
            elif command_parts[0] == '-':
                remove_repo(command_parts[1], repos_file)

    for t in threads:
        t.join()
