#!/bin/bash

# Check if running as root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root."
   exit 1
fi

# Set the service name
SERVICE_NAME="git-monitor"

# Set the paths
SERVICE_FILE="/etc/systemd/system/$SERVICE_NAME.service"
REPO_FILE="/etc/git-monitor/repos.txt"

# Stop and disable the service
systemctl stop $SERVICE_NAME
systemctl disable $SERVICE_NAME

# Remove the service file
rm -f $SERVICE_FILE

# Remove the symlinks
rm -f /usr/local/bin/git-monitor-server
rm -f /usr/local/bin/git-monitor-client

# Ask the user if they want to remove the repository file
read -p "Do you want to remove the repository file ($REPO_FILE)? (y/n): " REMOVE_REPO_FILE

if [[ $REMOVE_REPO_FILE =~ ^[Yy]$ ]]; then
   rm -f $REPO_FILE
fi

# Reload systemd to ensure it detects the service removal
systemctl daemon-reload

echo "Uninstallation completed."
